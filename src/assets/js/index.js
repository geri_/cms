function tabs_event() {
    $(".x-conteudo__menuTabs label").on('click', function(ev){
        var alvo = ev.currentTarget;
        var id_alvo = alvo.id;
        if(id_alvo == "tab-one"){
            $("#tab-1").slideDown();
            $("#tab-2").slideUp();
            $("#tab-3").slideUp();
            $("#tab-4").slideUp();
        } else if(id_alvo == "tab-two"){
            $("#tab-2").slideDown();
            $("#tab-1").slideUp();
            $("#tab-3").slideUp();
            $("#tab-4").slideUp();
        } else if(id_alvo == "tab-three"){
            $("#tab-3").slideDown();
            $("#tab-1").slideUp();
            $("#tab-2").slideUp();
            $("#tab-4").slideUp();
        }else{
            $("#tab-4").slideDown();
            $("#tab-1").slideUp();
            $("#tab-2").slideUp();
            $("#tab-3").slideUp();
        }
    })
};
function alerts(){
    $(".x-saveButton").on('click', function(){
        alert("Salvo com sucesso!");
    })
}
function modais(){
    $(".x-saveButton").on('click', function(){
        $(".x-sucess--save").css('display','flex');
    })
    $(".x-sucess--save span").on('click', function(){
        $(".x-sucess--save").css('display','none');
        location.reload();
    })
}
tabs_event();
// alerts();
modais();