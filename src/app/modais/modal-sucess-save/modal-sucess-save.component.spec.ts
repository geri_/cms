import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSucessSaveComponent } from './modal-sucess-save.component';

describe('ModalSucessSaveComponent', () => {
  let component: ModalSucessSaveComponent;
  let fixture: ComponentFixture<ModalSucessSaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSucessSaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSucessSaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
