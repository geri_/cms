import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-modal-sucess-save',
  templateUrl: './modal-sucess-save.component.html',
  styleUrls: ['./modal-sucess-save.component.css']
})
export class ModalSucessSaveComponent implements OnInit, AfterContentChecked {
  public select: string;
  public modalAtivo: string;

  constructor(private generalService: GeneralService) { }

  ngOnInit() {
  }
  public abrirAba(aba_v) {
    this.generalService.abas(aba_v);
  }
  public nmArquivo(file) {
    this.generalService.nmArquivo(file);
  }
  public previewImg(imagem) {
    this.generalService.previewImg(imagem);
  }
  public abrirModal(modal) {
    this.generalService.abrirModal(modal);
  }
  ngAfterContentChecked(){
    this.modalAtivo = this.generalService.modalAtivo;
  }
}
