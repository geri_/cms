import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSucessSentComponent } from './modal-sucess-sent.component';

describe('ModalSucessSentComponent', () => {
  let component: ModalSucessSentComponent;
  let fixture: ComponentFixture<ModalSucessSentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSucessSentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSucessSentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
