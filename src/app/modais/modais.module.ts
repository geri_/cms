import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalSucessSaveComponent } from './modal-sucess-save/modal-sucess-save.component';
import { ModalSignUpComponent } from './modal-sign-up/modal-sign-up.component';
import { ModalSucessSentComponent } from './modal-sucess-sent/modal-sucess-sent.component';

@NgModule({
  declarations: [
    ModalSucessSaveComponent,
    ModalSignUpComponent,
    ModalSucessSentComponent
  ],
  exports: [
    ModalSucessSaveComponent,
    ModalSignUpComponent,
    ModalSucessSentComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ModaisModule { }
