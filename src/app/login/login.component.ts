import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';
import * as $ from 'jquery';

//Classes
import { Usuario } from './class/usuario';

//Services
import { SignService } from './sign.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit, AfterContentChecked {
  public modalAtivo: string;
  private username: Usuario = new Usuario();
  private password: string;
  private logado: string;

  constructor(private generalService: GeneralService, private signService: SignService) { }

  ngOnInit() {
    window.sessionStorage.setItem('logado', 'false');
    this.generalService.background("url('cidade.jpg')");
  }
  public abrirModal(modal) {
    this.generalService.abrirModal(modal);
  }
  fazerLogin(){
    this .signService.fazerLogin(this.username);
  }
  ngAfterContentChecked(){
    this.modalAtivo = this.generalService.modalAtivo;
    // this.username = this.generalService.username;
    // this.password = this.generalService.password;
    this.logado = this.generalService.logado;
  }
}
