import { GeneralService } from 'src/app/principal/general.service';
import { Injectable, EventEmitter } from '@angular/core';
import { Usuario } from './class/usuario';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SignService {

  mostrarMenuEmitter = new EventEmitter <boolean>();

  private usuarioAutenticado: boolean = false;

  constructor(private router: Router, private generalService: GeneralService) { }
  fazerLogin(usuario: Usuario){
    if(usuario.nome === 'usuario@email.com' && usuario.senha === '123456'){
      this.usuarioAutenticado = true;
      this.mostrarMenuEmitter.emit(true);
      this.router.navigate(['/home']);
      this.generalService.background("#fff");
    } else{
      this.usuarioAutenticado = false;
      this.mostrarMenuEmitter.emit(false);
      alert('Usuario não encontrado')
    }
  }
}
