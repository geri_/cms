import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';

// Components
import { AppComponent } from './app.component';

//Services
import { GeneralService } from './principal/general.service';

//Routes
import { RoutesRoutingModule } from './routes/routes-routing.module';

//Modules
import { LoginComponent } from './login/login.component';
import { SystemModule } from './components/system.module';
import { SignService } from './login/sign.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    SystemModule,
    RoutesRoutingModule,
    FormsModule
  ],
  providers: [GeneralService, SignService],
  bootstrap: [AppComponent]
})
export class AppModule { }
