import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';

@Component({
  selector: 'app-conteudo',
  templateUrl: './conteudo.component.html',
  styleUrls: ['./conteudo.component.css']
})
export class ConteudoComponent implements OnInit {

  public sub_menu: boolean;
  public menu: string;

  constructor(public generalService: GeneralService) { }

  ngOnInit() {
  }
  public show(item) {
    this.generalService.showSubMenu(item);
  }
  public hide() {
    this.generalService.hideSubMenu();
  }
}
