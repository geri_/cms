import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-white-label',
  templateUrl: './white-label.component.html',
  styleUrls: ['./white-label.component.less']
})
export class WhiteLabelComponent implements OnInit, AfterContentChecked {
  public select: string;
  public aba: string;
  public modalAtivo: string;

  constructor(private generalService: GeneralService) { }

  ngOnInit() {
  }
  public abrirAba(aba_v) {
    this.generalService.abas(aba_v);
  }
  public nmArquivo(file) {
    this.generalService.nmArquivo(file);
  }
  public previewImg(imagem) {
    this.generalService.previewImg(imagem);
  }
  public abrirModal(modal) {
    this.generalService.abrirModal(modal);
  }
  ngAfterContentChecked(){
    this.select = this.generalService.select;
    this.aba = this.generalService.abas_select;
    this.modalAtivo = this.generalService.modalAtivo;
  }
}
