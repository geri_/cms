import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuLateralComponent } from './menu-lateral/menu-lateral.component';
import { SubMenuComponent } from './menu-lateral/sub-menu/sub-menu.component';

@NgModule({
  declarations: [
    MenuLateralComponent,
    SubMenuComponent
  ],
  exports: [
    MenuLateralComponent,
    SubMenuComponent
  ],
  imports: [
    CommonModule
  ]
})
export class MenuModule { }
